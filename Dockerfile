FROM gcr.io/google_appengine/php72:2019-10-29-12-03

COPY nginx.conf /nginx/nginx.conf
COPY build-scripts/composer.sh /build-scripts/composer.sh

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
    gcc autoconf && \
    pecl install xdebug && \
    pecl clear-cache && \
    echo 'zend_extension=xdebug.so' > /opt/php72/lib/ext.enabled/ext-xdebug.ini && \
    /bin/bash /build-scripts/apt-cleanup.sh

RUN curl -L -o phpunit https://phar.phpunit.de/phpunit-8.4.phar && \
    chmod +x phpunit && \
    mv phpunit /usr/bin/phpunit

ONBUILD COPY composer.json composer.lock $APP_DIR

ONBUILD RUN chown -R www-data.www-data $APP_DIR

ONBUILD RUN /build-scripts/composer.sh

ENTRYPOINT ["/build-scripts/entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

ENV WHITELIST_FUNCTIONS 'exec, proc_open, shell_exec, system'

